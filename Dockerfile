FROM debian
RUN apt-get update && apt-get install -y \
  git \
  j2cli \
  openssh-client \
  bsd-mailx \
  openssl 
